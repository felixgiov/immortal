package ferari.felixgiov.immortal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class FragmentHome extends Fragment {

    Button tips;
    Button pertopert;
    Button riwayat;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_home, container, false);

        tips = (Button)view.findViewById(R.id.buttontips);
        pertopert = (Button) view.findViewById(R.id.buttonp3k);
        riwayat= (Button) view.findViewById(R.id.buttonhapem);

        riwayat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ia = new Intent(getActivity(), RiwayatPemActivity.class);
                startActivity(ia);
            }
        });

        tips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), TipsActivity.class);
                startActivity(i);
            }
        });

        pertopert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ix = new Intent(getActivity(), PertolonganPertamaActivity.class);
                startActivity(ix);
            }
        });
        return view;
    }

}
