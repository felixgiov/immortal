package ferari.felixgiov.immortal;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisActivity extends AppCompatActivity {

    RadioGroup profil;
    RadioButton radioButton;
//    DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
//    DatabaseReference mjobRef = mRootRef.child("job");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regis);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String str = new String("Ibu Hamil");
        profil = (RadioGroup)findViewById(R.id.rad_group);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get selected radio button from radioGroup
                int selectedId = profil.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);
//                mjobRef.setValue(radioButton.getText());

                if(radioButton.getText().equals("Ibu Hamil")){
                    startActivity(new Intent(view.getContext(), AdvRegisActivity.class));
                    finish();
                }else if(radioButton.getText().equals("Relawan")){
                    startActivity(new Intent(view.getContext(), RelawanMainActivity.class));
                    finish();
                }else{
                    startActivity(new Intent(view.getContext(), MedicalMainActivity.class));
                    finish();
                }
            }
        });
    }

}
