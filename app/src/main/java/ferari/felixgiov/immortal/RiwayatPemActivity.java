package ferari.felixgiov.immortal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ferari.felixgiov.immortal.Recycler.MessageViewHolder;
import ferari.felixgiov.immortal.Recycler.RiwayatViewHolder;

public class RiwayatPemActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    DatabaseReference mDatabase;
    DatabaseReference mDatabaseChat;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_pem);

        linearLayoutManager = new LinearLayoutManager(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseChat = mDatabase.child("riwayat");

        mRecyclerView = (RecyclerView) findViewById(R.id.rvriw);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onStart() {
        super.onStart();

        //    Log.d("nsja",mDatabase.toString());

        FirebaseRecyclerAdapter<RiwayatPemeriksaan, RiwayatViewHolder> adapter = new FirebaseRecyclerAdapter<RiwayatPemeriksaan, RiwayatViewHolder>(
                RiwayatPemeriksaan.class,
                R.layout.riwayatpemeriksaantab,
                RiwayatViewHolder.class,
                mDatabaseChat
        ) {
            @Override
            protected void populateViewHolder(RiwayatViewHolder viewHolder, RiwayatPemeriksaan riw, int position) {
                viewHolder.tanggalpem.setText(riw.getTanggalpem());
                viewHolder.tensipem.setText(riw.getTensipem());
                viewHolder.nadipem.setText(riw.getNadipem());
                viewHolder.respirasipem.setText(riw.getRespirasipem());
                viewHolder.suhupem.setText(riw.getSuhupem());
                viewHolder.statusinterna.setText(riw.getStatusinterna());
                viewHolder.statuskehamilan.setText(riw.getStatuskehamilan());
                viewHolder.gravidopem.setText(riw.getGravidopem());
                viewHolder.gpem.setText(riw.getGpem());
                viewHolder.ppem.setText(riw.getPpem());
                viewHolder.apem.setText(riw.getApem());
                viewHolder.therapypem.setText(riw.getTherapypem());

            }
        };
        mRecyclerView.setAdapter(adapter);
    }
}
