package ferari.felixgiov.immortal;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.File;

public class ProfileActivity extends AppCompatActivity {

    Button signout;
    TextView namalengkap;
    TextView emailprof;
    String nama;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        signout = (Button) findViewById(R.id.signout);
        namalengkap = (TextView) findViewById(R.id.namadiprofile);
        emailprof = (TextView) findViewById(R.id.emaildiprofile);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        nama = pref.getString("namasp", null);
        email = pref.getString("emailsp", null);

        namalengkap.setText(nama);
        emailprof.setText(email);

        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
//                SharedPreferences.Editor editor = pref.edit();
//                editor.clear();
//                editor.commit();
//
//                SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//                SharedPreferences.Editor editor2 = getPrefs.edit();
//                editor2.clear();
//                editor2.commit();
                clearApplicationData();
                finish();
                System.exit(0);
            }
        });
    }

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if(appDir.exists()){
            String[] children = appDir.list();
            for(String s : children){
                if(!s.equals("lib")){
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "File /data/data/APP_PACKAGE/" + s +" DELETED");
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }
}
