package ferari.felixgiov.immortal;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AddNewRiwayatActivity extends AppCompatActivity {

    EditText tanggalpemet;
    EditText tensipemet;
    EditText nadipemet;
    EditText respirasipemet;
    EditText suhupemet;
    EditText statusinternaet;
    EditText statuskehamilanet;
    EditText gravidopemet;
    EditText gpemet;
    EditText ppemet;
    EditText apemet;
    EditText therapypemet;

     String tanggalpem;
     String tensipem;
     String nadipem;
     String respirasipem;
     String suhupem;
     String statusinterna;
     String statuskehamilan;
     String gravidopem;
     String gpem;
     String ppem;
     String apem;
     String therapypem;

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_riwayat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

         tanggalpemet = (EditText) findViewById(R.id.tanggalpemet);
        tensipemet = (EditText) findViewById(R.id.tensiet);
         nadipemet = (EditText) findViewById(R.id.nadiet);
         respirasipemet = (EditText) findViewById(R.id.respirasiet);
         suhupemet = (EditText) findViewById(R.id.suhuet);
         statusinternaet = (EditText) findViewById(R.id.statusinternaet);
         statuskehamilanet = (EditText) findViewById(R.id.statuskehamilanet);
         gravidopemet = (EditText) findViewById(R.id.gravidoet);
         gpemet = (EditText) findViewById(R.id.get);
         ppemet = (EditText) findViewById(R.id.pet);
         apemet = (EditText) findViewById(R.id.aet);
         therapypemet = (EditText) findViewById(R.id.theraphyet);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabsend);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 tanggalpem = tanggalpemet.getText().toString();
                 tensipem = tensipemet.getText().toString();
                 nadipem  = nadipemet.getText().toString();
                 respirasipem = respirasipemet.getText().toString();
                 suhupem = suhupemet.getText().toString();
                 statusinterna = statusinternaet.getText().toString();
                 statuskehamilan = statuskehamilanet.getText().toString();
                 gravidopem = gravidopemet.getText().toString();
                 gpem = gpemet.getText().toString();
                 ppem = ppemet.getText().toString();
                 apem = apemet.getText().toString();
                 therapypem = therapypemet.getText().toString();

                writeNewRiwayat(tanggalpem,  tensipem,  nadipem,  respirasipem,  suhupem,  statusinterna,  statuskehamilan,  gravidopem,  gpem,  ppem,  apem,  therapypem);
                finish();
            }
        });
    }

    private void writeNewRiwayat(String tanggalpem, String tensipem, String nadipem, String respirasipem, String suhupem, String statusinterna, String statuskehamilan, String gravidopem, String gpem, String ppem, String apem, String therapypem) {

        String userId = UUID.randomUUID().toString();

        String key = mDatabase.child("riwayat").push().getKey();
        RiwayatPemeriksaan riw = new RiwayatPemeriksaan( tanggalpem,  tensipem,  nadipem,  respirasipem,  suhupem,  statusinterna,  statuskehamilan,  gravidopem,  gpem,  ppem,  apem,  therapypem );
        Map<String, Object> postValues = riw.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/riwayat/" + key, postValues);
        //    childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
        // mDatabase.child("jobs").child(userId).setValue(user);
    }

}
