package ferari.felixgiov.immortal;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import ferari.felixgiov.immortal.Recycler.MessageViewHolder;

import static android.content.Context.MODE_PRIVATE;

public class FragmentRela extends Fragment {

    RecyclerView mRecyclerView;
    DatabaseReference mDatabase;
    DatabaseReference mDatabaseChat;
    EditText textmessagebumil;
    Button tombolkirimbumil;
    String nama;
    String email;
    LinearLayoutManager linearLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_rela, container, false);

        SharedPreferences pref = getActivity().getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        nama = pref.getString("namasp", null);
        email = pref.getString("emailsp", null);

        textmessagebumil = (EditText)view.findViewById(R.id.textmessagetvbumilrela);
        tombolkirimbumil = (Button)view.findViewById(R.id.sendbumilrela);

        tombolkirimbumil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNewMessage(nama,email,textmessagebumil.getText().toString());
                textmessagebumil.setText("");

                InputMethodManager inputManager = (InputMethodManager)
                        getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);

            }
        });

        linearLayoutManager = new LinearLayoutManager(getActivity());

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseChat = mDatabase.child("chat-relawan");

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rvbumilrela);
        mRecyclerView.setHasFixedSize(true);
        linearLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        //    Log.d("nsja",mDatabase.toString());

        FirebaseRecyclerAdapter<Chat, MessageViewHolder> adapter = new FirebaseRecyclerAdapter<Chat, MessageViewHolder>(
                Chat.class,
                R.layout.chattab,
                MessageViewHolder.class,
                mDatabaseChat
        ) {
            @Override
            protected void populateViewHolder(MessageViewHolder viewHolder, Chat chat, int position) {
//                viewHolder.setName(chat.getName());
//                viewHolder.setText(chat.getText());
                viewHolder.mName.setText(chat.getName());
//                Log.d("nanananana",chat.getName());
                viewHolder.mMessage.setText(chat.getMessage());
            }
        };
        mRecyclerView.setAdapter(adapter);
    }



    private void sendNewMessage(String name, String uid, String message) {

        //    String userId = UUID.randomUUID().toString();

        String key = mDatabase.child("chat-relawan").push().getKey();
        Chat chat = new Chat(name, uid, message);
        Map<String, Object> postValues = chat.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/chat-relawan/" + key, postValues);
        //    childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
        // mDatabase.child("jobs").child(userId).setValue(user);
    }

}
