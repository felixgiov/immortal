package ferari.felixgiov.immortal;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

import static android.R.id.message;

/**
 * Created by felix on 23 Okt 2016.
 */

public class Chat {
    private String name;
    private String message;
    private String uid;

    public Chat() {
    }

    public Chat(String name, String uid, String message) {
        this.name = name;
        this.message = message;
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public String getUid() {
        return uid;
    }

    public String getMessage() {
        return message;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("uid", uid);
        result.put("message", message);

        return result;
    }

}