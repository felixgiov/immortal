package ferari.felixgiov.immortal;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class TipsActivity extends AppCompatActivity {

    Button tips1;
    Button tips2;
    Button tips3;
    Button tips4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips);

        tips1 = (Button)findViewById(R.id.buttonkosntipasi);
        tips2 = (Button)findViewById(R.id.buttonkeputihan);
        tips3 = (Button)findViewById(R.id.buttonmual);
        tips4 = (Button)findViewById(R.id.buttonnyeri);

        tips1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        TipsActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Konstipasi/Sembelit");

                // Setting Dialog Message
                alertDialog.setMessage("Sembelit saat kehamilan merupakan hal yang wajar karena adaya penekanan daerah usur besar disebabkan adanya pembesaran rahim. Atasi sembelit dengan:\n" +
                        "\n" +
                        "- Minum air yang banyak minimal 8 kali sehari  ukura gelas minum\n" +
                        "- Istirahat yang cukup\n" +
                        "- Perbanyak konsumsi sayur dan buah\n" +
                        "- Latih pola BAB yang teratur \n" +
                        "- Lakukan aktivitas grak jalan untuk mengendorkan otot pencernaan\n\n" +
                        "(Varney, 2007)\n");
                alertDialog.show();
            }
        });

        tips2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        TipsActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Keputihan");

                // Setting Dialog Message
                alertDialog.setMessage("Untuk mengatasi keputihan pada saat hamil anda bisa melakuakan cara berikut:\n\n" +
                        "1.    Sebaiknya anda menghindari pemakaian air yang kotor ketika membasuh area kewanitaan anda, terutama juga kita menggunakan toilet umum. \n" +
                        "2.    Selalu gunakan pakaian dalam yang berbahan dasar katun selain itu hindari pemakaian celana dalam yang berbahan dasar sintetis juga hindaari celana yang ketat. \n" +
                        "3.    Sebaiknya anda tidak terlalu sering memakai panty liner pada setiap harinya. Panty liner dapat membuat area kewanitaan anda menjadi lembab. \n" +
                        "4.    Segeralah mengganti pakaian dalam anda ketika terasa sangat lembab.\n");
                alertDialog.show();
            }
        });

        tips3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        TipsActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Mual dan Muntah");

                // Setting Dialog Message
                alertDialog.setMessage("- Makan dengan porsi sedikit tapi sering\n" +
                        "- Plih makanan dalam bentuk kering (biskuit, roti)\n" +
                        "- Hindari minum bersamaan dengan makan (beri waktu jeda)\n" +
                        "- Hindari makanan pedas\n" +
                        "- Jangan membiarkan perut kosog\n" +
                        "- Hidari bau-bauan atau apaun yang merangsang mual muntah\n" +
                        "- Minum air putih yang bayak dan sesering mungkin\n" +
                        "- Konsultasi dengan tenaga kesehaan apabila muntah berlebih\n");
                alertDialog.show();
            }
        });

        tips4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        TipsActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Konstipasi/Sembelit");

                // Setting Dialog Message
                alertDialog.setMessage("Ada beberapa hal yang dapat Anda lakukan untuk membantu mencegah sakit punggung saat hamil. Beberapa hal yang bisa diterapkan antara lain seperti yang dikutip dari NHS berikut ini:\u000B\u000B1. Hindari mengangkat benda yang berat.\u000B2. Tekuk lutut dan menjaga punggung tetap lurus saat mengangkat atau mengambil sesuatu dari lantai.\u000B3. Gunakan kaki ketika Anda ingin berbalik untuk menghindari tulang belakang memutar.\u000B4. Memakai sepatu yang datar sehingga memungkinkan penyebaran beban berat badan Anda menjadi lebih merata.\u000B5. Saat membawa tas belanjaan, upayakan seimbang dalam membawanya. Bagilah beban di kedua tangan.\u000B6. Duduk dengan punggung lurus dan pastikan menggunakan dukungan punggung seperti bantal atau handuk yang digulung.\u000B7. Pastikan Anda istirahat cukup.\u000B8. Memijat punggung dengan lembut juga dapat membantu menghindari nyeri\n" +
                        "\n" +
                        "Sumber  http://health.detik.com/read/2014/03/26/102545/2536925/1299/nyeri-punggung-saat-hamil-ini-sebab-dan-cara-mengatasinya\n");
                alertDialog.show();
            }
        });
    }
}
