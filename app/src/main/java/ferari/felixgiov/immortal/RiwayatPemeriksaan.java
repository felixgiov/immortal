package ferari.felixgiov.immortal;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by felix on 27 Okt 2016.
 */

public class RiwayatPemeriksaan {

    public String tanggalpem;
    public String tensipem;
    public String nadipem;
    public String respirasipem;
    public String suhupem;
    public String statusinterna;
    public String statuskehamilan;
    public String gravidopem;
    public String gpem;
    public String ppem;
    public String apem;
    public String therapypem;

    public RiwayatPemeriksaan(){

    }

    public RiwayatPemeriksaan(String tanggalpem, String tensipem, String nadipem, String respirasipem, String suhupem, String statusinterna, String statuskehamilan, String gravidopem, String gpem, String ppem, String apem, String therapypem) {
        this.tanggalpem = tanggalpem;
        this.tensipem = tensipem;
        this.nadipem = nadipem;
        this.respirasipem = respirasipem;
        this.suhupem = suhupem;
        this.statusinterna = statusinterna;
        this.statuskehamilan = statuskehamilan;
        this.gravidopem = gravidopem;
        this.gpem = gpem;
        this.ppem = ppem;
        this.apem = apem;
        this.therapypem = therapypem;
    }

    public String getTanggalpem() {
        return tanggalpem;
    }

    public String getTensipem() {
        return tensipem;
    }

    public String getNadipem() {
        return nadipem;
    }

    public String getRespirasipem() {
        return respirasipem;
    }

    public String getSuhupem() {
        return suhupem;
    }

    public String getStatusinterna() {
        return statusinterna;
    }

    public String getStatuskehamilan() {
        return statuskehamilan;
    }

    public String getGravidopem() {
        return gravidopem;
    }

    public String getGpem() {
        return gpem;
    }

    public String getPpem() {
        return ppem;
    }

    public String getApem() {
        return apem;
    }

    public String getTherapypem() {
        return therapypem;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("tanggalpem", tanggalpem);
        result.put("tensipem", tensipem);
        result.put("nadipem", nadipem);
        result.put("respirasipem", respirasipem);
        result.put("suhupem", suhupem);
        result.put("statusinterna", statusinterna);
        result.put("statuskehamilan", statuskehamilan);
        result.put("gravidopem", gravidopem);
        result.put("gpem", gpem);
        result.put("ppem", ppem);
        result.put("apem", apem);
        result.put("therapypem", therapypem);

        return result;
    }

}
