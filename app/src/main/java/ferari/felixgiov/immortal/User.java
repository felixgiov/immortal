package ferari.felixgiov.immortal;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by felix on 20 Okt 2016.
 */

public class User {

    public String nama;
    public String email;
    public String job;
    public String nama_suami;
    public String alamat;
    public String agama;
    public String riwayat_hamil;
    public String riwayat_sakit;
    public String hamil_ke;
    public String goldar;
    public String lastmens;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String nama, String email, String job, String nama_suami, String alamat, String agama, String riwayat_hamil, String riwayat_sakit, String hamil_ke, String goldar, String lastmens) {
        this.nama = nama;
        this.email = email;
        this.job = job;
        this.nama_suami = nama_suami;
        this.alamat = alamat;
        this.agama = agama;
        this.riwayat_hamil = riwayat_hamil;
        this.riwayat_sakit = riwayat_sakit;
        this.hamil_ke = hamil_ke;
        this.goldar = goldar;
        this.lastmens = lastmens;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("nama", nama);
        result.put("email", email);
        result.put("job", job);
        result.put("nama_suami", nama_suami);
        result.put("alamat", alamat);
        result.put("agama", agama);
        result.put("riwayat_hamil", riwayat_hamil);
        result.put("riwayat_sakit", riwayat_sakit);
        result.put("hamil_ke", hamil_ke);
        result.put("goldar", goldar);
        result.put("lastmens", lastmens);

        return result;
    }
}
