package ferari.felixgiov.immortal;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;

public class Fragment_RS extends Fragment{

    Button callbutton;
    WebView petawv;
    Button lihatpeta;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment__r, container, false);

        petawv = (WebView) view.findViewById(R.id.petawv);

        lihatpeta = (Button) view.findViewById(R.id.peta_btn);
        callbutton = (Button) view.findViewById(R.id.call_btn);

        lihatpeta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                petawv.loadUrl("https://www.google.co.id/maps/search/rumah+sakit+umum/@-7.767637, 110.376476,17z?hl=id");
            }
        });

        callbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:0377778888"));
                startActivity(callIntent);
                SmsManager smsManager = SmsManager.getDefault();
                smsManager.sendTextMessage("+62812333333", null, "[IMMORTAL] Keadaan darurat! Ibu sedang membutuhkan pertolongan anda. Silahkan datang ke -6.3645997,106.8264028 ", null, null);
            }
        });
        return view;
    }
}
