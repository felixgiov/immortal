package ferari.felixgiov.immortal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class CariRelawanActivity extends AppCompatActivity {

    Button tambahrel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cari_relawan);

        tambahrel = (Button) findViewById(R.id.tambahrelawan);

        tambahrel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                Toast.makeText(CariRelawanActivity.this, "Relawan berhasil ditambahkan", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
