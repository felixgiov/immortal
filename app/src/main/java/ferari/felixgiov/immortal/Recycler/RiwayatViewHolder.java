package ferari.felixgiov.immortal.Recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ferari.felixgiov.immortal.R;
import ferari.felixgiov.immortal.RiwayatPemeriksaan;

/**
 * Created by felix on 27 Okt 2016.
 */

public class RiwayatViewHolder extends RecyclerView.ViewHolder {

    public TextView tanggalpem;
    public TextView tensipem;
    public TextView nadipem;
    public TextView respirasipem;
    public TextView suhupem;
    public TextView statusinterna;
    public TextView statuskehamilan;
    public TextView gravidopem;
    public TextView gpem;
    public TextView ppem;
    public TextView apem;
    public TextView therapypem;

    public RiwayatViewHolder(View v){
        super(v);

        tanggalpem = (TextView) v.findViewById(R.id.tanggalpemtv);
        tensipem = (TextView) v.findViewById(R.id.tensitv);
        nadipem = (TextView) v.findViewById(R.id.naditv);
        respirasipem = (TextView) v.findViewById(R.id.respirasitv);
        suhupem= (TextView) v.findViewById(R.id.suhutv);
        statusinterna= (TextView) v.findViewById(R.id.statusinternatv);
        statuskehamilan= (TextView) v.findViewById(R.id.statuskehamilantv);
        gravidopem= (TextView) v.findViewById(R.id.gravidotv);
        gpem= (TextView) v.findViewById(R.id.gtv);
        ppem= (TextView) v.findViewById(R.id.ptv);
        apem= (TextView) v.findViewById(R.id.atv);
        therapypem = (TextView) v.findViewById(R.id.theraphytv);
    }

}
