package ferari.felixgiov.immortal.Recycler;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ferari.felixgiov.immortal.R;

/**
 * Created by felix on 25 Okt 2016.
 */

public class MessageViewHolder extends RecyclerView.ViewHolder {
    //      View mView;
    public TextView mName;
    public TextView mMessage;

    public MessageViewHolder(View v) {
        super(v);
        //         mView = v;
        mName = (TextView)v.findViewById(R.id.namachattv);
        mMessage = (TextView) v.findViewById(R.id.chattv);
    }

//        public void setName(String name) {
//            TextView field1 = (TextView) mView.findViewById(R.id.namachattv);
//            field1.setText(name);
//        }
//
//        public void setText(String text) {
//            TextView field2 = (TextView) mView.findViewById(R.id.chattv);
//            field2.setText(text);
//        }
}