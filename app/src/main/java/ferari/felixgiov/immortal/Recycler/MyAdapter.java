package ferari.felixgiov.immortal.Recycler;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ferari.felixgiov.immortal.Chat;
import ferari.felixgiov.immortal.R;

/**
 * Created by felix on 25 Okt 2016.
 */

public class MyAdapter extends RecyclerView.Adapter<MessageViewHolder> {

    Context c;
    ArrayList<Chat> chats;

    public MyAdapter(Context c, ArrayList<Chat> chats) {
        this.c = c;
        this.chats = chats;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.chattab,parent,false);
        MessageViewHolder holder = new MessageViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        String name = chats.get(position).getName();
        String message = chats.get(position).getMessage();
        holder.mName.setText(name);
        holder.mMessage.setText(message);
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }
}
