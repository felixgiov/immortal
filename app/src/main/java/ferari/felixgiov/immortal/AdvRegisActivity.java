package ferari.felixgiov.immortal;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class AdvRegisActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;

    EditText jobet;
    EditText nama_suamiet;
    EditText alamatet;
    EditText agamaet;
    EditText riwayat_hamilet;
    EditText riwayat_sakitet;
    EditText hamil_keet;
    EditText lastmenset;
    Button datepicker;
    RadioGroup goldarrg;
    EditText emaildokter;
    EditText emailrelawan;
    Button carirelawan;

    String nama;
    String email;
    String job2;
    String nama_suami2;
    String alamat2;
    String agama2;
    String riwayat_hamil2;
    String riwayat_sakit2;
    String hamil_ke2;
    String lastmens2;
    String emaildokterstr;
    String emailrelawanstr;

    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adv_regis);

        mDatabase = FirebaseDatabase.getInstance().getReference();

//        GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//        GoogleSignInAccount acct = result.getSignInAccount();

        pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        nama = pref.getString("namasp", null);
        email = pref.getString("emailsp", null);

        jobet = (EditText) findViewById(R.id.jobtv);
        nama_suamiet = (EditText) findViewById(R.id.nama_suamitv);
        alamatet = (EditText) findViewById(R.id.alamattv);
        agamaet = (EditText) findViewById(R.id.agamatv);
        riwayat_hamilet = (EditText) findViewById(R.id.riwayat_hamiltv);
        riwayat_sakitet = (EditText) findViewById(R.id.riwayat_sakittv);
        hamil_keet = (EditText) findViewById(R.id.hamil_ketv);
        lastmenset = (EditText) findViewById(R.id.lastmenstv);
        datepicker = (Button) findViewById(R.id.buttondatepicker);
        goldarrg = (RadioGroup) findViewById(R.id.goldarrg);
        carirelawan = (Button) findViewById(R.id.buttoncarirelawan);
        emaildokter = (EditText) findViewById(R.id.emaildokterkonsul);

//        nama = acct.getDisplayName();
//        email = acct.getEmail();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        datepicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                System.out.println("the selected " + mDay);
                DatePickerDialog dialog = new DatePickerDialog(AdvRegisActivity.this,
                        new mDateSetListener(), mYear, mMonth, mDay);
                dialog.show();
            }
        });

        carirelawan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), CariRelawanActivity.class);
                startActivity(i);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                job2 = jobet.getText().toString();
                nama_suami2 = nama_suamiet.getText().toString();
                alamat2 = alamatet.getText().toString();
                agama2 = agamaet.getText().toString();
                riwayat_hamil2 = riwayat_hamilet.getText().toString();
                riwayat_sakit2 = riwayat_sakitet.getText().toString();
                hamil_ke2 = hamil_keet.getText().toString();
                lastmens2 = lastmenset.getText().toString();
                emaildokterstr = emaildokter.getText().toString();
         //       emailrelawanstr = emailrelawan.getText().toString();
                String value = ((RadioButton)findViewById(goldarrg.getCheckedRadioButtonId() )).getText().toString();

                SharedPreferences.Editor editor2 = pref.edit();
                editor2.putString("emaildokter", emaildokterstr);
         //       editor2.putString("emailrelawan", emailrelawanstr);
                editor2.commit();

                Log.d("alalaalaalalala",job2);
                writeNewUser( nama, email, job2, nama_suami2, alamat2, agama2, riwayat_hamil2, riwayat_sakit2, hamil_ke2, value, lastmens2 );
                finish();
            }
        });
    }

    private void writeNewUser(String nama, String email, String job, String nama_suami, String alamat, String agama, String riwayat_hamil, String riwayat_sakit, String hamil_ke, String goldar, String lastmens) {

        String userId = UUID.randomUUID().toString();

        String key = mDatabase.child("user").push().getKey();
        User user = new User(nama, email, job, nama_suami, alamat, agama, riwayat_hamil, riwayat_sakit, hamil_ke, goldar, lastmens );
        Map<String, Object> postValues = user.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/user/" + key, postValues);
    //    childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
       // mDatabase.child("jobs").child(userId).setValue(user);
    }

    class mDateSetListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            // getCalender();
            int mYear = year;
            int mMonth = monthOfYear;
            int mDay = dayOfMonth;
            lastmenset.setText(new StringBuilder()
                    // Month is 0 based so add 1
                    .append(mDay).append("/").append(mMonth + 1).append("/")
                    .append(mYear).append(" "));
            System.out.println(lastmenset.getText().toString());


        }
    }

}
