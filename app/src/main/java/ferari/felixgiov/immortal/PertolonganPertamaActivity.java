package ferari.felixgiov.immortal;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PertolonganPertamaActivity extends AppCompatActivity {

    Button tips1;
    Button tips2;
    Button tips3;
    Button tips4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pertolongan_pertama);

        tips1 = (Button)findViewById(R.id.buttonpendarahanless);
        tips2 = (Button)findViewById(R.id.buttonpendarahanmore);
        tips3 = (Button)findViewById(R.id.buttonracun);
        tips4 = (Button)findViewById(R.id.buttonpecah);

        tips1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        PertolonganPertamaActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Pendarahan saat hamil < 5 bulan");

                // Setting Dialog Message
                alertDialog.setMessage("Peringatan!\n\n" +
                        "Segera datang atau hubungi petugas atau layanan kesehatan untuk pertolongan selanjutnya.\n" +
                        "\n" +
                        "Tanda dan gejala\n\n" +
                        "- Darah yang keluar dari kemaluan biasanya disertai gumpalan\n" +
                        "- Darah yang keluar disertai nyer perut yang memilin\n" +
                        "\n" +
                        "Penanganan Dini\n\n" +
                        "- Istirahat total dengan merebahkan diri\n" +
                        "- Jika pasien dalam kondisi sadar, beri air putih atau minuman elektrolit sebanyak mungkin \n");
                alertDialog.show();
            }
        });

        tips2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        PertolonganPertamaActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Pendarahan saat hamil > 5 bulan");

                // Setting Dialog Message
                alertDialog.setMessage("Peringatan!\n\n" +
                        "Segera datang atau hubungi petugas atau layanan kesehatan untuk pertolongan selanjutnya.\n" +
                        "\n" +
                        "Tanda dan gejala\n\n" +
                        "- Plasenta previa/ plasenta letak rendah : pendarahan tanpa nyeri, yang keluar adalah darah segar berwarna merah, dan tidak ada kontraksi perut\n" +
                        "- Solusio Plasebta/plasenta terlepas dari tempatnya : pendarahan dengan nyeri menetap, warna darah kehitaman dan cair, tetapi mungkin ada bekua jika ada trauma, kuku berwarna biru keabu-abuan, uterus tegang terus menerus dan nyeri\n\n" +
                        "Penanganan Dini\n\n" +
                        "- Istirahat total dengan merebahkan diri\n" +
                        "- Jika pasien dalam kondisi sadar, beri air putih atau minuman elektrolit sebanyak mungkin \n");
                alertDialog.show();
            }
        });

        tips3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        PertolonganPertamaActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Keracunan Kehamilan");

                // Setting Dialog Message
                alertDialog.setMessage("Peringatan!\n\n" +
                        "Segera datang atau hubungi petugas atau layanan kesehatan untuk pertolongan selanjutnya.\n" +
                        "\n" +
                        "Tanda dan gejala\n\n" +
                        "- Bengkak pada wajah, tangan, kaki\n" +
                        "- Ada sakit kepala\n" +
                        "- Kejang maupun tanpa kejang\n" +
                        "- Gangguan penglihatan\n\n" +
                        "Penanganan Dini\n\n" +
                        "- Jika sudah kejang, jaga agar lidah pasien tidak tergigit, jalan nafas bebasm dan tidak mengalami benturan atau trauma. Bisa dengan sendok dibebat dengan kain hingga tebal dan gigitkan  pada ibu hamil\n" +
                        "- Segera laporkan ke petugas kesehatan\n");
                alertDialog.show();
            }
        });

        tips4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(
                        PertolonganPertamaActivity.this).create();

                // Setting Dialog Title
                alertDialog.setTitle("Ketuban Pecah Dini");

                // Setting Dialog Message
                alertDialog.setMessage("Peringatan!\n\n" +
                        "Segera datang atau hubungi petugas atau layanan kesehatan untuk pertolongan selanjutnya.\n" +
                        "\n" +
                        "Tanda dan gejala\n\n" +
                        "- Keluar cairan melalui vagina dengan ciri berbau amis dan berwarna putih keruh\n" +
                        "- Jika merembes akan seperti keputiha dan celana dalam basah secara terus menerus\n" +
                        "- Jika banyak terasa seperti ingin BAK\n" +
                        "\n" +
                        "Penanganan Dini\n\n" +
                        "- Tirah baring (berbaring pada posisi yang nyaman diatas ranjang dan tidak melakukan aktivitas berat)\n" +
                        "- Laporkan pada petugas kesehatan\n");
                alertDialog.show();
            }
        });

    }
}
