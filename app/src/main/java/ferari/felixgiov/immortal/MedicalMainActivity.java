package ferari.felixgiov.immortal;

import android.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;
import java.util.Map;

import ferari.felixgiov.immortal.Recycler.MessageViewHolder;

public class MedicalMainActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    DatabaseReference mDatabase;
    DatabaseReference mDatabaseChat;
    Query chatQuery;
    EditText textmessagemedic;
    Button tombolkirimmedic;
    String nama;
    String email;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_main);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor2 = pref.edit();
        editor2.putBoolean("medicsp", true);
        nama = pref.getString("namasp", null);
        email = pref.getString("emailsp", null);
        editor2.commit();

        textmessagemedic = (EditText)findViewById(R.id.textmessagetvmedic);
        tombolkirimmedic = (Button)findViewById(R.id.sendmedic);

        tombolkirimmedic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNewMessage(nama,email,textmessagemedic.getText().toString());
                textmessagemedic.setText("");

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

        linearLayoutManager = new LinearLayoutManager(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseChat = mDatabase.child("chat");

        chatQuery = mDatabase.child("chat").equalTo("felixgiov");

        mRecyclerView = (RecyclerView) findViewById(R.id.rvmedic);
        mRecyclerView.setHasFixedSize(true);
        linearLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onStart() {
        super.onStart();

        //    Log.d("nsja",mDatabase.toString());

        FirebaseRecyclerAdapter<Chat, MessageViewHolder> adapter = new FirebaseRecyclerAdapter<Chat, MessageViewHolder>(
                Chat.class,
                R.layout.chattab,
                MessageViewHolder.class,
                mDatabaseChat
        ) {
            @Override
            protected void populateViewHolder(MessageViewHolder viewHolder, Chat chat, int position) {
//                viewHolder.setName(chat.getName());
//                viewHolder.setText(chat.getText());
                viewHolder.mName.setText(chat.getName());
//                Log.d("nanananana",chat.getName());
                viewHolder.mMessage.setText(chat.getMessage());
            }
        };
        mRecyclerView.setAdapter(adapter);
    }

    private void sendNewMessage(String name, String uid, String message) {

        //    String userId = UUID.randomUUID().toString();

        String key = mDatabase.child("chat").push().getKey();
        Chat chat = new Chat(name, uid, message);
        Map<String, Object> postValues = chat.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/chat/" + key, postValues);
        //    childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
        // mDatabase.child("jobs").child(userId).setValue(user);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_dokter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_about_medic_relawan2:

                Intent profile = new Intent(this, ProfileActivity.class);
                startActivity(profile);

                return true;

            case R.id.action_add_riw:

                Intent ia = new Intent(this, AddNewRiwayatActivity.class);
                startActivity(ia);

                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


}
