package ferari.felixgiov.immortal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.HashMap;
import java.util.Map;

import ferari.felixgiov.immortal.Recycler.MessageViewHolder;

public class RelawanMainActivity extends AppCompatActivity {

    RecyclerView mRecyclerView;
    DatabaseReference mDatabase;
    DatabaseReference mDatabaseChat;
    Query chatQuery;
    EditText textmessagerelawan;
    Button tombolkirimrelawan;
    String nama;
    String email;
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relawan_main);

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor2 = pref.edit();
        editor2.putBoolean("relawansp", true);
        nama = pref.getString("namasp", null);
        email = pref.getString("emailsp", null);
        editor2.commit();

        textmessagerelawan = (EditText)findViewById(R.id.textmessagetvrelawan);
        tombolkirimrelawan = (Button)findViewById(R.id.sendrelawan);

        tombolkirimrelawan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNewMessage(nama,email,textmessagerelawan.getText().toString());
                textmessagerelawan.setText("");

                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });

        linearLayoutManager = new LinearLayoutManager(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseChat = mDatabase.child("chat-relawan");

        chatQuery = mDatabase.child("chat-relawan").equalTo("felixgiov");

        mRecyclerView = (RecyclerView) findViewById(R.id.rvrelawan);
        mRecyclerView.setHasFixedSize(true);
        linearLayoutManager.setStackFromEnd(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onStart() {
        super.onStart();

        //    Log.d("nsja",mDatabase.toString());

        FirebaseRecyclerAdapter<Chat, MessageViewHolder> adapter = new FirebaseRecyclerAdapter<Chat, MessageViewHolder>(
                Chat.class,
                R.layout.chattab,
                MessageViewHolder.class,
                mDatabaseChat
        ) {
            @Override
            protected void populateViewHolder(MessageViewHolder viewHolder, Chat chat, int position) {
//                viewHolder.setName(chat.getName());
//                viewHolder.setText(chat.getText());
                viewHolder.mName.setText(chat.getName());
//                Log.d("nanananana",chat.getName());
                viewHolder.mMessage.setText(chat.getMessage());
            }
        };
        mRecyclerView.setAdapter(adapter);
    }

    private void sendNewMessage(String name, String uid, String message) {

        //    String userId = UUID.randomUUID().toString();

        String key = mDatabase.child("chat-relawan").push().getKey();
        Chat chat = new Chat(name, uid, message);
        Map<String, Object> postValues = chat.toMap();
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/chat-relawan/" + key, postValues);
        //    childUpdates.put("/user-posts/" + userId + "/" + key, postValues);

        mDatabase.updateChildren(childUpdates);
        // mDatabase.child("jobs").child(userId).setValue(user);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_signout, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_about_medic_relawan:

                Intent profile = new Intent(this, ProfileActivity.class);
                startActivity(profile);

                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}
